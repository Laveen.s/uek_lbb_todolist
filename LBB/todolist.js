const express = require('express')
const session = require('express-session')
const app = express()
const port = 3000

const swaggerUi = require('swagger-ui-express')
const swaggerDocument = require('./swagger-output.json')

app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument))

// Liste Tasks von Chat GPT generiert
const tasks = [
  {
    description: 'Brot kaufen',
    creationDate: 'June 1, 2023',
    completionDate: 'June 10, 2023'
  },
  {
    description: 'Gym gehen',
    creationDate: 'June 2, 2023',
    completionDate: 'June 12, 2023'
  },
  {
    description: 'Post gehen',
    creationDate: 'June 3, 2023',
    completionDate: 'June 15, 2023'
  },
  {
    description: 'Schwimmen',
    creationDate: 'June 4, 2023',
    completionDate: 'June 20, 2023'
  }
]

app.use(express.urlencoded())
app.use(express.json({ extended: true }))

app.use(session({
  secret: 'seupersecret',
  resave: false,
  saveUninitialized: false,
  cookie: {}
}))

// Errorhandling mit Hilfe von ChatGPT
app.use((err, req, res, next) => {
  console.error(err.stack)
  res.status(500).json({ error: 'Internal Server Error' })
})

app.get('/', (request, response) => {
  response.send('Todolist')
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})

app.all('/tasks*', (request, response, next) => {
  if (!request.session.email) {
    return response.status(403).json({ Status: 'Forbidden' })
  }
  next()
})

app.get('/tasks', (request, response) => {
  response.json(tasks)
  // Vorlage für Swagger von https://levelup.gitconnected.com/
  /*
    #swagger.tags = ["definitions"]
    #swagger.description = 'Shows all Tasks'
  */

  /* #swagger.responses[200] = {
      description: "All Tasks shown",
      schema: { $ref: "#/definitions/task"}
    } */
})

app.post('/tasks', (request, response) => {
  const newTask = request.body
  if (!newTask.description) {
    return response.status(406).json({ Error: 'Not Acceptable, No Title' })
  }

  tasks.push(newTask)
  const id = tasks.findIndex((task) => task === newTask)

  response.status(201).json({ task: newTask, id })
  /*
    #swagger.tags = ["definitions"]
    #swagger.description = 'Created a New Task'
  */

  /* #swagger.responses[201] = {
      description: "New Task created",
      schema: { $ref: "#/definitions/task"}
    } */
})

app.get('/tasks/:id', (request, response) => {
  const id = request.params.id

  if (!tasks[id]) {
    return response.status(404).json({ Error: 'Not Found' })
  }

  const task = tasks[id]
  response.json(task)
  /*
    #swagger.tags = ["definitions"]
    #swagger.description = 'Gives Information about a specific Task'
  */

  /* #swagger.responses[200] = {
      description: "Information about a Task shown",
      schema: { $ref: "#/definitions/task"}
    } */
})

app.put('/tasks/:id', (request, response) => {
  const editTask = request.body
  const id = request.params.id
  if (!editTask.description) {
    return response.status(406).json({ Error: 'Not Acceptable, No Title' })
  }

  if (!tasks[id]) {
    return response.status(404).json({ Error: 'Not Found' })
  }

  tasks.splice(id, 1, editTask)

  response.json(editTask)
  /*
    #swagger.tags = ["definitions"]
    #swagger.description = 'Replaces a Task'
  */

  /* #swagger.responses[200] = {
      description: "Task replaced by another Task",
      schema: { $ref: "#/definitions/task"}
    } */
})

app.delete('/tasks/:id', (request, response) => {
  const id = request.params.id

  if (!tasks[id]) {
    return response.status(404).json({ Error: 'Not found' })
  }

  response.json(tasks[id])

  tasks.splice(id, 1)
  /*
    #swagger.tags = ["definitions"]
    #swagger.description = 'Deletes a Task'
  */

  /* #swagger.responses[200] = {
      description: "Task deleted",
      schema: { $ref: "#/definitions/task"}
    } */
})

app.post('/login', (request, response) => {
  const { email, password } = request.body

  if ((email === 'laveen@gmail.com' && password === 'm295') || (email === 'lav@gmail.com' && password === 'm295')) {
    request.session.email = email
    response.json({ Email: email, Login: 'Successful' })
  } else {
    response.status(401).json({ Login: 'Unauthorized' })
  }
  /*
    #swagger.tags = ["definitions"]
    #swagger.description = 'To authenticate'
  */

  /* #swagger.responses[200] = {
      description: "Logged in",
      schema: { $ref: "#/definitions/task"}
    } */
})

app.get('/verify', (request, response) => {
  if (request.session.email) {
    response.status(200).json({ status: 'Logged in', email: request.session.email })
  } else {
    response.status(401).json({ status: 'Not Logged in' })
  }
  /*
    #swagger.tags = ["definitions"]
    #swagger.description = 'Checks if your logged in'
  */

  /* #swagger.responses[200] = {
      description: "Your are Logged in",
      schema: { $ref: "#/definitions/task"}
    } */
})

app.delete('/logout', (request, response) => {
  request.session.email = undefined
  response.sendStatus(204)
  /*
    #swagger.tags = ["definitions"]
    #swagger.description = 'Logs out'
  */

  /* #swagger.responses[204] = {
      description: "Logged out",
      schema: { $ref: "#/definitions/task"}
    } */
})
