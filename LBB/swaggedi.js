const swaggerAutogen = require('swagger-autogen')()
const outputFile = './swagger-output.json'
const endpointsFiles = ['./todolist.js']

const config = {
  info: {
    title: 'Todolist',
    description: 'In this Todolist you can write your Tasks'
  },
  host: 'localhost:3000',
  securityDefinitions: {
    api_key: {
      type: 'apiKey',
      name: 'api-key',
      in: 'header'
    }
  },
  schemes: ['http'],
  definitions: {
    'server side error': {
      $status: 'ERROR',
      $msg: 'some error message',
      error: {
        $message: 'Error message caught',
        $name: 'Error name',
        stack: 'Error stack'
      }
    },
    calculation: {
      $createdAt: '2020-03-31T00:00:00.000Z',
      $result: 100
    },
    task: {
      $description: 'Eat protein',
      $creationDate: 'June 4, 2023',
      $completionDate: 'June 5, 2023'
    }
  }
}

swaggerAutogen(outputFile, endpointsFiles, config).then(async () => {
  await import('./todolist.js') // Your express api project's root file where the server starts
})
