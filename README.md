<!--- Vorlage ChatGPT --->
# Todolist

Bei diesem Projekt kann man seine täglichen Aufgaben eintrage als Todolist.

## Inhaltsverzeichnis

- [Projektbeschreibung](#projektbeschreibung)
- [Installation](#installation)
- [Verwendung](#verwendung)
- [Autoren](#autoren)

## Projektbeschreibung

Dieses Projekt habe ich erstellt für die LBB im UEK 295. 

## Installation

Sie brauchen Postman für dieses Projekt

## Verwendung

Man kann sich einloggen und dann Todos erstellen.

## Autoren

Laveen Selvareswaran
